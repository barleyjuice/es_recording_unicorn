# Unicorn EEG recording plugin for Experiment Suite Core #

Windows-compatible module for EEG-signal recording from Unicorn Hybrid Black BCI. The module connects to Unicorn BCI, acquires data corresponding to traditional channel naming and writes them into csv log file. Optionally, any information about stimuli event occurence can be also added in a log within `marker_channel` column. Currently there can be only one event channel.

## Installation ##

To install the module as dependency in your Python project simply type:
```
pip install git+https://barleyjuice@bitbucket.org/barleyjuice/es_recording_unicorn.git#egg=es_recording_unicorn
```
If the installation was successful, you will be able to import the module in your project:
```python
from es_recording.unicorn import UnicornRecorder
```

## Use ##

Firstly create `UnicornRecorder` object and pass in constructor `marker_channel` parameter - name of the additional column in recording log where information about event occurence will be placed:

```python
recorder = UnicornRecorder(marker_channel="EVENT_MARKER")
```
Then the above constructed `UnicornRecorder` object in `recorder` variable can be passed as input parameter to create `Application` instance:

```python
# assume that `experimentScheme` and `presenter` are constructed avove
app = Application(
    presenter=presenter, 
    experimentScheme=experimentScheme, 
    recorder=recorder)
```
