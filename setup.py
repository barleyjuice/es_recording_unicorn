from setuptools import setup

setup(
    name='es_recording_unicorn',
    version='0.1.0',
    author='Aliona Petrova',
    author_email='consciencee95@gmail.com',
    packages=['es_recording.unicorn'],
    install_requires=[
        'unicorn-api@git+https://barleyjuice@bitbucket.org/barleyjuice/unicorn-api.git',
        # 'es_core',
    ],
    # dependency_links=[
    #     "git+https://barleyjuice@bitbucket.org/barleyjuice/unicorn-api.git#egg=unicorn-api"
    # ],
    url='https://bitbucket.org/barleyjuice/es_recording_unicorn/',
    description='Unicorn EEG recording plugin for Experiment Suite Core',
    long_description=open('README.md').read(),
    include_package_data=True
)
