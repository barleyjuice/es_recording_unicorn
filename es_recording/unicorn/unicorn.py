from unapi import Unicorn
from es_core.recording import Recorder

class UnicornRecorder(Recorder):

    def __init__(self, marker_channel) -> None:
        self.bci = Unicorn()
        (devices, devices_cnt) = self.bci.getAvailableDevices()
        if devices_cnt == 0:
            raise RuntimeError("No any device is available!")
        self.serial = devices[0]
        self.handlerId = self.bci.openDevice(self.serial)
        self.config = self.bci.getConfiguration(self.handlerId)
        self.channels = list(map(lambda ch: str(ch.name, 'utf-8'), sorted(self.config.channels, key=lambda x: x.index)))
        if marker_channel:
            self.channels.append(marker_channel)
        super().__init__(channels=self.channels, marker_channel=marker_channel)

    def log_data(self):
        self.bci.startAcquisition(self.handlerId)
        while True:
            if not self.enabled:
                break
            data = self.bci.getData(self.handlerId, n_samples=1)
            current_event_cnt = self.event_cnt
            data_write = [str(sample) for (i, sample) in enumerate(data)]
            if self.marker_channel:
                data_write.append(str(self.current_event))
                self.clear_event(current_event_cnt)
            self.log.write(",".join(data_write) + "\n")
        self.bci.stopAcquisition(self.handlerId)
        #self.wait()

    def terminate(self):
        super().terminate()
        self.bci.closeDevice(self.handlerId)